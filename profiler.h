#ifndef _PROFILE_H_
#include <time.h>

class Profiler {
private:
  // Valores posibles (ver más en man clock_gettime
  // CLOCK_REALTIME
  //        System-wide clock that measures real (i.e., wall-clock) time.  Setting this clock requires  appropriate  privileges.
  //        This clock is affected by discontinuous jumps in the system time (e.g., if the system administrator manually changes
  //        the clock), and by the incremental adjustments performed by adjtime(3) and NTP.
  // CLOCK_REALTIME_COARSE (since Linux 2.6.32; Linux-specific)
  //        A faster but less precise version of CLOCK_REALTIME.  Use when you need very fast, but not fine-grained timestamps.
  // CLOCK_MONOTONIC
  //        Clock that cannot be set and represents monotonic time since some unspecified starting point.  This clock  is
  //        not  affected  by  discontinuous jumps in the system time (e.g., if the system administrator manually changes
  //        the clock), but is affected by the incremental adjustments performed by adjtime(3) and NTP.
  // CLOCK_MONOTONIC_COARSE (since Linux 2.6.32; Linux-specific)
  //        A faster but less precise version of CLOCK_MONOTONIC.  Use when you need  very  fast,  but  not  fine-grained
  //        timestamps.
  // CLOCK_MONOTONIC_RAW (since Linux 2.6.28; Linux-specific)
  //        Similar  to  CLOCK_MONOTONIC,  but  provides  access  to a raw hardware-based time that is not subject to NTP
  //        adjustments or the incremental adjustments performed by adjtime(3).
  // CLOCK_BOOTTIME (since Linux 2.6.39; Linux-specific)
  //        Identical to CLOCK_MONOTONIC, except it also includes any time that the system  is  suspended.   This  allows
  //        applications  to  get  a  suspend-aware  monotonic  clock  without  having  to deal with the complications of
  //        CLOCK_REALTIME, which may have discontinuities if the time is changed using settimeofday(2).
  // CLOCK_PROCESS_CPUTIME_ID (since Linux 2.6.12)
  //        High-resolution per-process timer from the CPU.
  // CLOCK_THREAD_CPUTIME_ID (since Linux 2.6.12)
  //        Thread-specific CPU-time clock.// 
  
  clockid_t _clock;
  struct timespec _start, _stop;
  bool _started;
  double _result;
  
public:
  // Constructor
  // Arranca el perfilado
  Profiler(clockid_t clock=CLOCK_MONOTONIC_COARSE);
  void start();
  // Detiene la depuración
  void stop();
  // Muestra los resultados
  double result();
};

#endif
