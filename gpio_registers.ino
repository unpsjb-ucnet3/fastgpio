#include "debugger.h"
#include "profiler.h"

// extern struct fgpio_sc fgpio;

Profiler p;
Dbg dbg;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  for (int i=0; i<16; i++) {
    pinMode(i, INPUT);  
  }
}

int val;

void loop() {
  
  for (int i=0; i<16; i++) {
    p.start();
    val = fastGpioDigitalRead(i);
    p.stop();
    dbg("fastGpioDigitalRead(")(i)(") =")(val & 1)("en")(p.result())("uS")++;
  }

  for (int i=0; i <16; i++) {
    p.start(); 
    val = digitalRead(i);
    p.stop();
    dbg("digitalRead(")(i)(") =")(val)("en")(p.result())("uS")++;
  }
  
  usleep(1000000);
}
