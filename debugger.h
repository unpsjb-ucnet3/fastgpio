#include <variant.h> // Definición de extern Serial

class Dbg
{
public:
    // Constructor

    // Operador () con cadenas
    Dbg& operator()(char const *s){
        //printf("%s ",  s);
        Serial.print(s);
        Serial.print(" ");
        return *this;
    }
    // Operador () con enteros
    Dbg& operator()(int i){
        Serial.print(i);
        Serial.print(" ");
        return *this;
    }
    
    // El ++ avanza línea
    Dbg& operator++(){
        Serial.print("\n");
        return *this;
    }
    // Avanzar una línea
    Dbg& operator++(int){
        this->operator++();
        return *this;
    }
};

