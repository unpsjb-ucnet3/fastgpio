#include "profiler.h"
#include <strings.h>

Profiler::Profiler(clockid_t clock) {
  _started = false;
  _result = 0;
  bzero(&_start, sizeof(_start));
  bzero(&_stop, sizeof(_stop));  
}

void Profiler::start() {
  if (!_started) {
    _started = true;
    clock_gettime(_clock, &_start);
  }
}

void Profiler::stop() {
  if (_started) {
    clock_gettime(_clock, &_stop);
    _result = (_stop.tv_sec - _start.tv_sec) * 1e6 + (_stop.tv_nsec - _start.tv_nsec) / 1e3;
    _started = false;
  }
}

double Profiler::result(){
  return _result;
}
